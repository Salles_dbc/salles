public class Personagem {
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected int experiencia;
    protected int dobroExperiencia;
    protected Inventario inventarioElfoVerde;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.inventario = new Inventario(0);
        this.experiencia = 0;
        this.dobroExperiencia = 0;        
    }
    
    public Personagem( String nome ) {
        this.nome = nome;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome( String nome ) {
        this.nome = nome;
    }
    
    public Status getStatus(){
        return this.status;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
    public void ganharItem( Item item ) {
        this.inventario.adicionar( item );
    }
    
    public void perderItem( Item item ) {
        this.inventario.adicionar(item);
    }
    
    public void ganharODobroDeExperienciaElfoVerde(){
        this.dobroExperiencia = experiencia * 2;
    }
}