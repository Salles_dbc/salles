public class Dwarf extends Personagem {
    private boolean equipado;
    
    {
        this.inventario = new Inventario(1);
        this.equipado = false;
    }
    
    public Dwarf( String nome ) {
        super(nome);
        this.vida = 110.0;
        this.inventario.adicionar(new Item(1, "Escudo"));
    }
    
    private boolean podeSofrerDano() {
        return this.vida > 0;
    }
    
    public void sofrerDano() {
        if( podeSofrerDano() ){
            //this.vida -= 10.0;
            double qtdDano = this.equipado ? 5.0 : 10.0;
            this.vida -= qtdDano;
            status = Status.SOFREU_DANO;
            //this.vida = this.vida - 10.0;
        }else {
            status = Status.MORTO;
        }
    }
    
    public void equiparEscudo() {
        this.equipado = true;
    }
    
    /*public void mudarEscudo() {
        this.equipado = !this.equipado;
    }*/
    
    
    
    
    
}