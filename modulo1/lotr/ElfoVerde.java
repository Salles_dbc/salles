public class ElfoVerde extends Elfo {
    
    public ElfoVerde( String nome) {
       super(nome);
    }
    
    public void ganhaODobroDeExperiencia() {
        this.experiencia = experiencia * 2;        
    }
    
    public void podeGanharItem( Item item){
        this.inventario.adicionar(new Item(1, "Espada de aço valoriano"));
        this.inventario.adicionar(new Item(1, "Arco de vidro"));
        this.inventario.adicionar(new Item(1, "Flechas de vidro"));       
    }
    
    public void PodePerderItem(){
        this.inventario.adicionar(new Item(1, "Espada de aço valoriano"));
        this.inventario.adicionar(new Item(1, "Arco de vidro"));
        this.inventario.adicionar(new Item(1, "Flechas de vidro"));   
    }
}
