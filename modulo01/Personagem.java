public class Personagem {
    protected String nome;
    protected Status status;
    protected Inventario inventario;
    protected double vida;
    protected int experiencia;
    
    {
        this.status = Status.RECEM_CRIADO;
        this.experiencia = 0;
    }
    
    public Personagem( String nome ) {
        this.nome = nome;
    }
    
    public String getNome() {
        return this.nome;
    }
    
    public void setNome( String nome ) {
        this.nome = nome;
    }
    
    public Status getStatus(){
        return this.status;
    }
    
    public Inventario getInventario() {
        return this.inventario;
    }
    
    public double getVida() {
        return this.vida;
    }
    
    public int getExperiencia() {
        return this.experiencia;
    }
    
}