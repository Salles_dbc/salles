import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    
    @Test
    public void atirarFlechaDeveAumentarDobroXP() {
        ElfoVerde elfoVerde = new ElfoVerde("Elrond");
        Dwarf dwarf = new Dwarf("Gimli");
        elfoVerde.atirarFlecha(dwarf);
        assertEquals(2, elfoVerde.getQtdFlechas());
        assertEquals(4, elfoVerde.getExperiencia());
    }
    
    @Test
    public void elfoVerdePodeGanharItem() {
        ElfoVerde elfoQualquer = new ElfoVerde("Elrond");
        Item arcoVidro = new Item(1, "Arco de Vidro");
        elfoQualquer.ganharItem(arcoVidro);
        assertEquals(arcoVidro, elfoQualquer.getInventario().obter(2));
        assertEquals(2, elfoQualquer.getInventario().getItens().size());
    }
    
    @Test
    public void elfoVerdePodePerderItem() {
        ElfoVerde elfoQualquer = new ElfoVerde("Elrond");
        Item arcoVidro = new Item(1, "Arco de Vidro");
        elfoQualquer.ganharItem(arcoVidro);
        elfoQualquer.perderItem(arcoVidro);
        assertEquals(1, elfoQualquer.getInventario().getItens().size());
    } 

}
