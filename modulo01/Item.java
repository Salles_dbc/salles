public class Item {
    //quantidade, descricao
    private String descricao;
    private int quantidade;
    
    public Item(int quantidade, String descricao){
        this.quantidade = quantidade;
        this.descricao = descricao;
    }
    
    public String getDescricao() {
        return this.descricao;
    }
    
    public int getQuantidade() {
        return this.quantidade;
    }
    
    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }
    
    public boolean equals(Object obj) {
        Item outroItem = (Item) obj;
        return this.quantidade == outroItem.getQuantidade() &&
                this.descricao == outroItem.getDescricao();
    }
}