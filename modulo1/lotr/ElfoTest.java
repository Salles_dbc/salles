import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {

    @Test
    public void elfoDeveNascerCom2Flechas() {
        Elfo elfoQualquer = new Elfo("Legolas");
        assertEquals(2, elfoQualquer.getQtdFlechas());
    }
    
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXP() {
        Elfo elfoQualquer = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Malungrid");
        
        elfoQualquer.atirarFlecha(anao);
        assertEquals(1, elfoQualquer.getQtdFlechas());
        assertEquals(1, elfoQualquer.getExperiencia());
    }
    
    @Test
    public void atirar3FlechasDevePerderFlechaAumentarXP() {
        Elfo elfoQualquer = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Malungrid");
        //Acao
        elfoQualquer.atirarFlecha(anao);
        elfoQualquer.atirarFlecha(new Dwarf("Kronabela"));
        elfoQualquer.atirarFlecha(new Dwarf("Weramoren"));
        //Verificacao
        assertEquals(0, elfoQualquer.getQtdFlechas());
        assertEquals(2, elfoQualquer.getExperiencia());
    }
    
    @Test
    public void atirarFlechaEmDwarfTiraVida() {
        Elfo elfoQualquer = new Elfo("Legolas");
        Dwarf anao = new Dwarf("Malungrid");
        //Acao
        elfoQualquer.atirarFlecha(anao);
        //Verificacao
        assertEquals(1, elfoQualquer.getQtdFlechas());
        assertEquals(1, elfoQualquer.getExperiencia());
        assertEquals(100.0, anao.getVida(), .001);
    }
    
    
    
}